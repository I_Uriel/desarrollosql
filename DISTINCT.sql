DESCRIBE <table_name> /*muestra la estructura de laa tabla*/
DESC departments; /*devuelve el nombre de la tabla*/

SELECT department_id ||' '||  department_name
FROM departments;   /*department_id es concatenado al department_name*/


/*con el alias (As) devuelve la concatenacion con el nombre Employee Name*/
SELECT first_name ||' '||
last_name AS "Employee Name"   
FROM employees;

SELECT  last_name || ' has a monthly
salary of ' || salary || '
dollars.' AS Pay
FROM employees;

SELECT last_name ||' has a '|| 1 ||' year salary of '|| salary*12 ||
' dollars.' AS Pay
FROM employees;

/*DISTINCT se usa para eliminar filas duplicadas, la palabra DISTINC debe
aparecer despues de SELECT */
SELECT DISTINCT department_id
FROM employees;

