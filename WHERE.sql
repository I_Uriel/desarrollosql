/*WHERE es una codicion, en este caso muestra el id, nombre y apellido
con el id=101, se pueden usar los demas operadores de comparación*/
SELECT employee_id, first_name, last_name
FROM employees
WHERE employee_id = 101;

/*Los caracteres y fechas deben de ir entre comillas simples y distingue
entre mayusculas y minusculas*/
SELECT first_name, last_name
FROM employees
WHERE last_name = 'Taylor'; 

SELECT last_name, salary
FROM employees
WHERE salary <= 3000;



