/*De esta forma puedo copiar una tabla, debo de indicar que es una copia con copy_,
el cual sera el nombre de la tabla copiada, despues indico la tabla que se copiara
*/
CREATE TABLE copy_departments  
AS (SELECT * FROM departments); 

DESCRIBE copy_departments;    --Con esta instrucci�n verifico que la tabla se haya copiado
SELECT * FROM copy_departments; --Aqu� puedo ver los datos de la tabla copiada.

//
/*Insert se usa para gregar una fila, esta instruccion requiere de tres valores:
el nombre de la tabla, los nosmbres de las columnas en la tabla a rellenar y los
valores correspondientes para cada columna*/

INSERT INTO copy_departments    --inserta una nueva fila en la tabla copy_departmeents
(department_id, department_name, manager_id, location_id) --campos en donde se van a insertar los valores.
VALUES
(200,'Human Resources', 205, 1500);        --valores a insertar en los campos antes indicados