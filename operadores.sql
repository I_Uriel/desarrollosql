/*Operadores, BETWEEN muestra el salario entre 9000 y 11000*/
SELECT last_name, salary
FROM employees
WHERE salary BETWEEN 9000 AND 11000;

/*Otra forma de optener el salario mayor o igual a 9000 y menor o igual a 11000 */
WHERE salary >= 9000 AND salary <=11000;

/*Se utiliza IN para probar si unn valor esta en un conjunto especifico
de valores*/
SELECT city, state_province, country_id
FROM locations
WHERE country_id IN('UK', 'CA')
--WHERE country_id = 'UK' OR country_id = 'CA'; --otra forma de optener el mismo resultado

/*LIKE se usa para obtener datos con poca información que tengamos,
en este ejemplo muestra todos los apellidos que tengas en su segunda 
letra una o*/
SELECT last_name
FROM employees
WHERE last_name LIKE '_o%';

/*La opcion ESCAPE se usa para indicar que % y _ son parte del valor que busco*/
SELECT last_name, job_id
FROM EMPLOYEES
WHERE job_id LIKE '%\_R%' ESCAPE '\';

/*IS NULL muestra si no esta asignado, IS NOT NULL muestra las filas que tienen un valor */
SELECT last_name, manager_id
FROM employees
WHERE manager_id IS NULL;

SELECT last_name, commission_pct
FROM employees
WHERE commission_pct IS NOT NULL;

