SELECT department_id, AVG(salary) --selecionamos el id departamento y AVG promedia el salario
FROM employees                    --tabla de empleados
GROUP BY department_id            --divide las filas en grupos
ORDER BY department_id;           --usa las fuciones del grupo para resumir informacion