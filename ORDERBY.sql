/*ORDER BY, nos sirve para ordenar los datos en forma ascendente */
SELECT last_name, hire_date
FROM employees
ORDER BY hire_date;

/*DESC se usa para ordenar los datos en forma descendente */
SELECT last_name, hire_date
FROM employees
ORDER BY hire_date DESC;

/*En este caso se ordena por last_name aunque SELECT no lo indique as�*/
SELECT employee_id, first_name
FROM employees
WHERE employee_id < 105
ORDER BY last_name;

/*puede unirse con otras ccondiciones*/
SELECT department_id, last_name
FROM employees
WHERE department_id <= 50
ORDER BY department_id, last_name;

