/*LOWER convierte la primera letra a mayuscula, ya que la base de datoss distingue
entre mayusculas y miusculas me sirve para buscar un dato de la forma correcta*/
SELECT last_name
FROM employees
WHERE LOWER(last_name) = 'abel';

/*SUBSTR extrae una cadena de caracteres primero se indica la posici�n y despues 
la longuitud a estraer en este caso el resyltado es elloW  */
SELECT SUBSTR('HelloWorld',2, 5)
FROM DUAL;

/*LENGTH muestra la longuitud de una cadena la devuelve como un numero, en este
caso devuelve la longuitd del apellido de la tabla empleados*/
SELECT LENGTH(last_name)
FROM employees;

/*INSTR devuelve la posicion en valor numero del caracter solicitado
en este caso devolvera 6*/
SELECT INSTR('HelloWorld', 'W')
FROM DUAL;

/*REPLACE reemplza los caracteres inicados, primero se indica la cadena a remplacer
y despues los valores que se insertaran y reemplazaran*/
SELECT REPLACE('JACK and JUE', 'J', 'BL')
FROM DUAL;