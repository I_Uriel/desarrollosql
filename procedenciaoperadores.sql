--En esta parte se ve la procedencia de los operadores aritmticos
SELECT last_name, salary,
12*salary +100
FROM employees;
/*En esta primera parte hace la operacion segun el orden gerargico 
de los operadores, en este caso multiplaca 12 por el salirio y a ese 
resultado le suma 100*/


SELECT last_name, salary,
12*(salary +100)
FROM employees;
/*En esta segunda parte resuelve primero la operacion que esta
dentro del parentesis y despues lo multiplica por 12*/