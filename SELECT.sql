SELECT*           --EL comando SELECT devuelve todas las  filas de una tabla.
FROM employees;   --devuelve las filas de la tabla employees

SELECT first_name, last_name, job_id  -- Para devolver un subcojunto de datos
FROM employees                        --tabla employees 
WHERE job_id = 'SA_REP';              --condición

